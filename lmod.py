import os
import tempfile
import subprocess

def module(lmod_link, command, *arguments):
    """Wrapper for Lmod commands in python3.

    command -- load, unload etc.
    arguments -- module name (optional)

    This method works by invoking Lmod with a 'python3' argument, causing Lmod to return a set of python3 statements
    which set up the environment and print output if Lmod was invoked normally. The method then executes these statements.

    This is a python3 translation of the file 'env_modules_python.py' provided with the Lmnod package.

    """

    _, commandsFile = tempfile.mkstemp()

    modules_to_load = str.join(" ", arguments)
    cmd = [lmod_link, "python", command, modules_to_load]
    subprocess.run(cmd,
                   stdout=open(commandsFile, "w"),
                   stdin=open('/dev/null'))
    with open(commandsFile, "r") as modules_commands:
        exec(modules_commands.read())