import os
import subprocess


def main(run_dir, config, build_dirname="build"):
    cmd = ['make', '-j', '12']
    make = subprocess.run(cmd,
                          stdout=open(os.path.join(
                              run_dir, build_dirname, config + "_build.out"), 'w'),
                          stderr=open(os.path.join(
                              run_dir, build_dirname, config + "_build.err"), 'w'),
                          stdin=open('/dev/null'),
                          cwd=os.path.join(run_dir, build_dirname, config),)
    return make
